package counter

import "sync/atomic"

type ICounter[i int32 | int64 | uint32 | uint64] interface {
	Set(new i) (old i)
	Add(delta i) (val i)
	Get() (val i)
}

// 32 bit int counter implementation of ICounter
type Counter32 struct {
	val int32
}

func NewCounter32(initVal int32) ICounter[int32] {
	return &Counter32{val: initVal}
}

func (c *Counter32) Set(new int32) (old int32) {
	return atomic.SwapInt32(&c.val, new)
}

func (c *Counter32) Add(delta int32) (val int32) {
	return atomic.AddInt32(&c.val, delta)
}

func (c *Counter32) Get() (val int32) {
	return atomic.LoadInt32(&c.val)
}

// 64 bit int counter implementation of ICounter
type Counter64 struct {
	val int64
}

func NewCounter64(initVal int64) ICounter[int64] {
	return &Counter64{val: initVal}
}

func (c *Counter64) Set(new int64) (old int64) {
	return atomic.SwapInt64(&c.val, new)
}

func (c *Counter64) Add(delta int64) (val int64) {
	return atomic.AddInt64(&c.val, delta)
}

func (c *Counter64) Get() (val int64) {
	return atomic.LoadInt64(&c.val)
}

// 32 bit uint counter implementation of ICounter
type CounterU32 struct {
	val uint32
}

func NewCounterU32(initVal uint32) ICounter[uint32] {
	return &CounterU32{val: initVal}
}

func (c *CounterU32) Set(new uint32) (old uint32) {
	return atomic.SwapUint32(&c.val, new)
}

func (c *CounterU32) Add(delta uint32) (val uint32) {
	return atomic.AddUint32(&c.val, delta)
}

func (c *CounterU32) Get() (val uint32) {
	return atomic.LoadUint32(&c.val)
}

// 64 bit uint counter implementation of ICounter
type CounterU64 struct {
	val uint64
}

func NewCounterU64(initVal uint64) ICounter[uint64] {
	return &CounterU64{val: initVal}
}

func (c *CounterU64) Set(new uint64) (old uint64) {
	return atomic.SwapUint64(&c.val, new)
}

func (c *CounterU64) Add(delta uint64) (val uint64) {
	return atomic.AddUint64(&c.val, delta)
}

func (c *CounterU64) Get() (val uint64) {
	return atomic.LoadUint64(&c.val)
}
